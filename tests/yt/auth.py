from rr_lib.yt.auth import get_youtube, auth_db
from rr_lib.yt.shared import get_my_channel_info

if __name__ == '__main__':
    app_name = 'vc-2020'
    channel_id = 'UC5vSBbGV8mrAlPTNbXGHqmg'
    app_name2 = 'vc-server2k19'
    channel_id2 = "UCBRFOofDHds0EgDn8dK7UKQ"

    yt1 = get_youtube(app_name, channel_id=channel_id, client_id=1)
    print(get_my_channel_info(yt1))

    auth_db.remove_channel_credentials()
    yt2 = get_youtube(app_name=app_name2, channel_id=channel_id2, client_id=1)
    print(get_my_channel_info(yt2))
