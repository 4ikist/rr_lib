import json
import logging
import os
import re
import sys

# we need separate log for config, because main logger use this config.
log = logging.getLogger('cfg')
log.setLevel(logging.ERROR)

hdr = logging.FileHandler('./init_cfg.log')
hdr.setFormatter(logging.Formatter('%(asctime)s |%(process)d| [%(levelname)s] %(message)s'))
log.addHandler(hdr)

EV_TEST = 'RR_TEST'
EV_CONFIG_FILE_PATH = 'RR_CONFIG_PATH'


def is_test_mode():
    return os.environ.get(EV_TEST, "false").strip().lower() in ("true", "1", "yes")


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        key = hash("%s%s%s" % (args, kwargs, hash(cls)))
        saved = cls._instances.get(key)
        if not saved:
            cls._instances[key] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[key]


class ConfigException(Exception):
    pass


env_ref_reg = re.compile('(?P<replace>\$\{(?P<var_name>[\d\w\-_]+)\})')


class Cfg:
    def __init__(self, data):
        self.data = data

    def process_env_vars(self, value):
        if not isinstance(value, str):
            return value
        m = env_ref_reg.findall(value)
        if m:
            replace, var_name = m[0]
            var_val = os.environ.get(var_name)
            if var_val is None:
                raise ConfigException(f'Need {var_name} environment variable')
            return str(value).replace(replace, var_val)
        return value

    def get(self, name, default_value=None):
        result = self.data.get(name, default_value)
        if isinstance(result, dict):
            log.info(f'CFG GET section: {name} -> [{result}]')
            return Cfg(result)

        result = self.process_env_vars(result)
        log.debug(f'CFG GET: {name} -> {result}')
        return result

    def __getitem__(self, item):
        return self.get(item)


class ConfigManager(metaclass=Singleton):
    def __init__(self, config_fn=None):
        if not config_fn:
            config_path = os.environ.get(EV_CONFIG_FILE_PATH, )
            if not config_path:
                raise ConfigException('%s is undefined :(' % EV_CONFIG_FILE_PATH)

            if is_test_mode():
                config_file = os.path.join(config_path, 'cfg_test.json')
            else:
                if config_path.endswith('.json'):
                    config_file = config_path
                else:
                    config_file = os.path.join(config_path, 'cfg.json')
        else:
            config_file = config_fn

        try:
            f = open(config_file, )
            raw_data = '\n'.join(f.readlines())
        except Exception as e:
            log.exception(e)
            log.error("Can not read config file %s" % config_file)
            sys.exit(-1)

        self.config_data = Cfg(json.loads(raw_data))
        self.config_file = config_file

        log.info(
            "LOAD CONFIG DATA FROM %s:\n%s" % (
                config_file,
                "\n".join(["\t%s: %s" % (k, v) for k, v in self.config_data.data.items()]))
        )

    def get(self, name, default_value=None):
        return self.config_data.get(name, default_value)
