import logging

from rr_lib.cm import cfg


def lgr(name, to_file=None):
    log = logging.getLogger(name)
    log.setLevel(log_cfg.get('severity') or logging.INFO)
    fmt = logging.Formatter(log_cfg.get('format') or default_log_format)
    sh = logging.StreamHandler()
    sh.setFormatter(fmt)
    log.addHandler(sh)
    if to_file:
        fh = logging.FileHandler(to_file)
        fh.setFormatter(fmt)
        log.addHandler(fh)
    return log


default_log_format = '%(asctime)s |%(process)d| %(name)s [%(levelname)s] %(message)s'
log_cfg = cfg.get('main', {}).get('log', {})
