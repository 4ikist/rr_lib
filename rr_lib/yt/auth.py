import json
import logging
import time
import uuid
from random import random, randint

import httplib2
from googleapiclient.discovery import build
from oauth2client.client import OAuth2WebServerFlow, OAuth2Credentials, Storage
from oauth2client.tools import argparser, run_flow

from rr_lib.cm import ConfigManager
from rr_lib.cm.manager import Singleton
from rr_lib.db import DBHandler
from rr_lib.yt.exceptions import YtAuthException
from rr_lib.yt.shared import get_my_channel_info

YOUTUBE_SCOPE = "https://www.googleapis.com/auth/youtube"
YOUTUBE_UPLOAD_SCOPE = "https://www.googleapis.com/auth/youtube.upload"
YOUTUBE_READ_WRITE_SSL_SCOPE = "https://www.googleapis.com/auth/youtube.force-ssl"
YOUTUBE_CHANNEL_AUDIT = "https://www.googleapis.com/auth/youtubepartner-channel-audit"
YOUTUBE_PARTNER = "https://www.googleapis.com/auth/youtubepartner"
YOUTUBE_READ_WRITE_SCOPE = "https://www.googleapis.com/auth/youtubepartner-channel-audit"

YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

name = 'yt_auth'
log = logging.getLogger(name)
auth_config = ConfigManager().get(name)
default_client = auth_config.get('client_id', 1)


class _DatabaseConnector(DBHandler):
    def __init__(self):
        super(_DatabaseConnector, self).__init__(name=name, connection_name=name)
        self.apps = self.db.get_collection('app_creds')
        if not self.apps:
            self.apps = self.db.create_collection('app_creds')
            self.apps.create_index('name')
            self.apps.create_index('type')

        self.channels = self.db.get_collection('chan_creds')
        if not self.channels:
            self.channels = self.db.create_collection('chan_creds')
            self.channels.create_index([('app_name', 1), ('channel_id', 1)], unique=True)
            self.channels.create_index('flow_id')
            self.channels.create_index('current', )

    def add_app_credentials(self, name, type, data):
        q = {'name': name, }
        upd = {'type': type, 'data': data}
        return self.apps.update_one(q, {'$set': {**q, **upd}}, upsert=True)

    def get_app_credentials(self, name, ):
        found = self.apps.find_one({'name': name})
        if found is not None:
            data = found.get('data')
            type = found.get('type')
            if data and type in data:
                return data[type]

    def get_all_app_credentials(self):
        return self.apps.find({}, projection={'_id': False})

    def get_app_channels(self, app_name):
        return self.channels.find({'app_name': app_name}, projection={'_id': False})

    def delete_app_credentials(self, name):
        self.apps.delete_one({'name': name})
        self.channels.delete_many({'app_name': name})

    def add_channel_credentials(self, app_name, channel_id, flow_id, data, client_id=default_client):
        log.info(f'Add or update chan [{channel_id}] credentials for app [{app_name}]. '
                 f'Flow_id: {flow_id} | client_id: {client_id}')

        return self.channels.update_one({'channel_id': channel_id, 'app_name': app_name},
                                        {'$set': {'data': data,
                                                  'app_name': app_name,
                                                  'flow_id': flow_id,
                                                  'by_client': client_id}}, upsert=True)

    def get_channel_credentials(self, channel_id=None, app_name=None, flow_id=None):
        if flow_id:
            q = {'flow_id': flow_id}
        elif channel_id and app_name:
            q = {'channel_id': channel_id, 'app_name': app_name}
        else:
            raise KeyError('Or flow_id or app_name + channel_id')

        found = self.channels.find_one(q)
        if found:
            return found.get('data')

    def link_channel_credentials(self, flow_id, app_name, channel_id):
        exists_cred = self.channels.find_one({'app_name': app_name, 'channel_id': channel_id})
        if exists_cred:
            log.info(f'I have credentials: \n flow: {exists_cred.get("flow_id")}\n {exists_cred["data"]}')
            clear = self.channels.update_one({"flow_id": flow_id}, {"$unset": {'flow_id': True}})
            log.info(f'Cleared: {clear.raw_result}')
        else:
            scr = self.channels.update_one({"flow_id": flow_id}, {"$set": {'channel_id': channel_id}})
            log.info(f'link... set channel {scr.modified_count} docs')
            ufr = self.channels.update_one({"flow_id": flow_id}, {"$unset": {'flow_id': True}})
            log.info(f'link... delete stuff {ufr.modified_count} docs')

    def delete_channel_credentials(self, channel_id):
        return self.channels.delete_one({'channel_id': channel_id})

    def set_current_channel(self, channel_id, client_id=default_client):
        self.channels.update_one({'current': client_id}, {'$unset': {'current': channel_id}})
        q = {'channel_id': channel_id}
        result = self.channels.update_one(q, {'$set': dict({'current': client_id}, **q)}, upsert=True)
        return result

    def get_current_channel_id(self, client_id=default_client):
        found = self.channels.find_one({'current': client_id})
        if found:
            return found.get('channel_id')

    def unset_current_channel(self, client_id=default_client):
        return self.channels.update_one({'current': client_id}, {'$unset': {'current': 1}})

    def get_all_channels_creds(self):
        return list(self.channels.find({}))

    def remove_channel_credentials(self, channel_id=None, app_name=None):
        q = {}
        if channel_id:
            q['channel_id'] = channel_id
        if app_name:
            q['app_name'] = app_name

        return self.channels.delete_many(q)


auth_db = _DatabaseConnector()


class _channelCredentialStorage(Storage):
    def __init__(self, app_name, channel_id, client_id=default_client):
        super(_channelCredentialStorage, self).__init__()
        self.client_id = client_id
        self.app_name = app_name
        self.exp_channel_id = channel_id
        self.flow_id = str(uuid.uuid4())

    def locked_get(self):
        if self.exp_channel_id:
            data = auth_db.get_channel_credentials(channel_id=self.exp_channel_id, app_name=self.app_name)
            log.info("Found credentials by channel_id [%s] %s" % (self.exp_channel_id, data))
        else:
            data = auth_db.get_channel_credentials(flow_id=self.flow_id)
            log.info("Found credentials by flow [%s] %s" % (self.flow_id, data))
        if not data:
            return None
        return OAuth2Credentials.from_json(json.dumps(data))

    def locked_put(self, credentials):
        raw_data = credentials.to_json()
        log.info(f'Will store credentials app: {self.app_name}; '
                 f'channel_id: {self.exp_channel_id}, flow:{self.flow_id}\n{raw_data}')
        auth_db.add_channel_credentials(self.app_name,
                                        self.exp_channel_id,
                                        self.flow_id,
                                        json.loads(raw_data),
                                        client_id=self.client_id)

    def link(self, channel_id):
        auth_db.link_channel_credentials(self.flow_id, self.app_name, channel_id)


random_port = lambda: randint(7778, 8887)


class YoutubeAuthEngine:
    def __init__(self, app_name, client_id=default_client, auth_port=None):
        self.app_name = app_name
        self.client_id = client_id

        self._auth_port = auth_port or random_port()

        if isinstance(self.app_name, list):
            self._apps_iter = iter(self.app_name)
            self._current_app = next(self._apps_iter)
        else:
            self._current_app = self.app_name

    def authorise(self, storage):
        app_creds = auth_db.get_app_credentials(self._current_app)
        if not app_creds:
            raise Exception(f"You must create valid app creds for [{self._current_app}] by installing ")
        scope = [YOUTUBE_SCOPE, YOUTUBE_READ_WRITE_SCOPE, YOUTUBE_UPLOAD_SCOPE, YOUTUBE_CHANNEL_AUDIT, YOUTUBE_PARTNER,
                 YOUTUBE_READ_WRITE_SSL_SCOPE]
        log.info(f'app creds for is: {app_creds}')
        flow = OAuth2WebServerFlow(
            app_creds['client_id'],
            app_creds['client_secret'],
            scope,
            **{
                'redirect_uri': f"http://localhost:{self._auth_port}",
                'auth_uri': app_creds['auth_uri'],
                'token_uri': app_creds['token_uri'],
                'login_hint': "3030 SAY YES FOR THIS!!!",
            }
        )

        args, _ = argparser.parse_known_args()
        log.info(f'args: {args}')
        args.auth_host_port = [self._auth_port, ]
        channel_creds = run_flow(flow, storage, args)
        log.info(f'channel creds : {channel_creds}')
        del (flow)
        return channel_creds

    def __get_credentials_for_channel(self, channel_id):
        storage = _channelCredentialStorage(app_name=self._current_app,
                                            channel_id=channel_id,
                                            client_id=self.client_id)
        credentials = storage.get()
        if not credentials:
            log.info('Channel credentials were not found, authorising')
            credentials = self.authorise(storage)
        else:
            log.info('Channel credentials was found!')
            credentials = OAuth2Credentials.from_json(credentials.to_json())
            credentials.set_store(storage)

            if credentials.invalid:
                log.info('But invalid')
                credentials = self.authorise(storage)

            if credentials.access_token_expired:
                log.info('But token expired')
                credentials.refresh(httplib2.Http())

        storage.put(credentials)
        return credentials, storage

    def authenticate(self, channel_id):
        log.info(f'Start auth by [{self._current_app}]')
        credentials, storage = self.__get_credentials_for_channel(channel_id)

        yt = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                   http=credentials.authorize(httplib2.Http()))

        channel_info = get_my_channel_info(yt)
        if not channel_info:
            log.warning("Can not retrieve channel info...")
            if hasattr(self, '_apps_iter'):
                try:
                    self._current_app = next(self._apps_iter)
                    log.info(f'Try to use another app: [{self._current_app}]...')
                except StopIteration:
                    raise YtAuthException(f'Apps ended. Can not create yt :( ')

                time.sleep(3)
                return self.authenticate(channel_id)

            raise YtAuthException(f'Can not make yt :( ')

        storage.link(channel_info.get('channel_id'))
        del (credentials)

        return yt, channel_info


def ensure_apps(config_file):
    """
    Importing and saving
    :param config_file: file content must be {app_name: <app client_secret file contents from google console>}
    :return: list of {app_name, app_type, inserted id}
    """
    results = []
    with open(config_file) as f:
        apps_data = json.load(f)
        for app_name, app_data in apps_data.items():
            for app_type, _ in app_data.items():
                add_cred = auth_db.add_app_credentials(app_name, app_type, app_data)
                results.append(
                    (app_name, app_type, add_cred.upserted_id,)
                )

    return results


class _auth_process(object, metaclass=Singleton):

    def __init__(self, app_name, channel_id, client_id=default_client):
        auth_engine = YoutubeAuthEngine(app_name, client_id=client_id)
        yt, channel = auth_engine.authenticate(channel_id)
        self.yt = yt
        self.channel = channel
        del (auth_engine)


def get_youtube(app_name=None, channel_id=None, client_id=default_client):
    app_name = app_name or auth_config.get('app_name')
    if not app_name:
        raise Exception("Not app name :( ")
    channel_id = channel_id or auth_db.get_current_channel_id(client_id=client_id) or auth_config.get('channel_id')

    _yt_engine = _auth_process(app_name, channel_id, client_id)

    auth_db.set_current_channel(_yt_engine.channel.get('channel_id'), client_id=client_id)
    time.sleep(1)

    return _yt_engine.yt
