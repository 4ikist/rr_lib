import datetime
import logging
import time

from googleapiclient.errors import HttpError

log = logging.getLogger('yt_shared')
tryings_count = 3
sleep_between_try = 2


def net_tryings(fn):
    def wrapped(*args, **kwargs):
        count = 0
        while 1:
            try:
                result = fn(*args, **kwargs)
                return result
            except HttpError as e:
                log.exception(e)
                if e.resp.get('status') in ("400", "403", "404"):
                    return None
            except Exception as e:
                log.exception(e)
                log.warning("can not load data for [%s]\n args: %s, kwargs: %s \n because %s" % (fn, args, kwargs, e))
                if count >= tryings_count:
                    raise e
                time.sleep(sleep_between_try)
                count += 1

    return wrapped


def next_page_iter(list_func, q):
    while 1:
        result = net_tryings(list_func(**q).execute)()
        if not result:
            log.info('No any result in %s %s' % (q, list_func))
            break

        items = result.get("items", [])
        if items:
            for item in items:
                yield item
        else:
            break

        if not result.get("nextPageToken"):
            break
        else:
            q['pageToken'] = result.get("nextPageToken")


@net_tryings
def get_my_channel_info(yt):
    results = yt.channels().list(
        part="snippet",
        mine="true"
    ).execute()
    items = results.get('items')
    if items:
        snippet = items[0].get('snippet')
        if snippet:
            return {"title": snippet.get("title"),
                    "description": snippet.get("description"),
                    "channel_id": items[0].get("id")}


def yt_date_to_tst(yt_date):
    return time.mktime(datetime.datetime.strptime(yt_date, "%Y-%m-%dT%H:%M:%SZ").timetuple())


COMMENTS_CHUNK_SIZE = 40
VIDEOS_CHUNK_SIZE = 50


def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]
