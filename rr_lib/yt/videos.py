import re

from rr_lib.log import lgr
from rr_lib.yt.shared import net_tryings, yt_date_to_tst, chunks, VIDEOS_CHUNK_SIZE

log = lgr(__name__)

duration_obj_to_seconds = lambda x: 3600 * int(x['hours'] or 0) + \
                                    60 * int(x['minutes'] or 0) + \
                                    int(x['seconds'] or 0)

duration_reg = re.compile(u"PT((?P<hours>\d+)H)?((?P<minutes>\d+)M)?((?P<seconds>\d+)S)?")


def _parse_time(duration_str):
    for d in duration_reg.finditer(duration_str):
        result = d.groupdict()
        result = dict(map(lambda x: (x[0], int(x[1]) if x[1] else None), result.items()))
        return result


def get_duration(duration_str):
    str_time = _parse_time(duration_str)
    if str_time:
        return duration_obj_to_seconds(str_time)
    return None

@net_tryings
def get_many_videos(yt, video_ids, raw_result=False, part=None, exclude_fields=None):
    video_ids = list(set(video_ids))
    _part = part or "snippet,statistics,contentDetails,id"
    for chunk in chunks(video_ids, VIDEOS_CHUNK_SIZE):
        log.info("Will load from YT:\n%s" % ", ".join(chunk))
        cur_load_videos_data = yt.videos().list(
            **{"id": ",".join(chunk), "part": _part}).execute()

        for video in cur_load_videos_data.get('items', []):
            if raw_result:
                parts = _part.split(',')
                video_data = {}
                for part in parts:
                    if part == 'id':
                        update = {'video_id': video.get(part)}
                    else:
                        update = video.get(part, {})
                    video_data.update(update)

                if exclude_fields:
                    for field in exclude_fields:
                        if field in video_data:
                            video_data.pop(field)

                for k, v in video.items():
                    if 'count' in k.lower():
                        video[k] = int(v)

                if 'duration' in video:
                    video['duration'] = get_duration(video['duration'])
                if 'publishedAt' in video:
                    video['publishedAt'] = yt_date_to_tst(video['publishedAt'])
                if 'recordingDate' in video and video['recordingDate']:
                    video['recordingDate'] = yt_date_to_tst(video['recordingDate'])

            else:
                snippet = video.get("snippet", {})
                statistics = video.get("statistics", {})
                content_details = video.get("contentDetails", {})
                if snippet and statistics and content_details:
                    video_data = {
                        "video_id": video.get("id"),
                        "channel_id": snippet.get("channelId"),
                        "channel_title": snippet.get("channelTitle"),
                        "img": snippet.get("thumbnails", {}).get("default", {}).get("url"),
                        "title": snippet.get("title"),
                        "description": snippet.get("description"),
                        "yt_likes": int(statistics.get('likeCount', 0)),
                        "yt_views": int(statistics.get('viewCount', 0)),
                        "yt_dislikes": int(statistics.get('dislikeCount', 0)),
                        "yt_favorites": int(statistics.get('favoriteCount', 0)),
                        "time": yt_date_to_tst(snippet.get("publishedAt")),
                        "duration": get_duration(content_details.get('duration'))
                    }
                else:
                    log.warning('Video [%s] have not data' % video.get('id'))
                    continue

            yield video_data
